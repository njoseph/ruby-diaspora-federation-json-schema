#########################################################
# This file has been automatically generated by gem2tgz #
#########################################################
# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "diaspora_federation-json_schema"
  s.version = "0.2.4"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Benjamin Neff", "cmrd Senya"]
  s.date = "2018-02-18"
  s.description = "This gem provides JSON schemas (currently one schema) for validating JSON serialized federation objects."
  s.email = ["benjamin@coding4.coffee", "senya@riseup.net"]
  s.files = ["lib/diaspora_federation/schemas.rb", "lib/diaspora_federation/schemas/federation_entities.json"]
  s.homepage = "https://github.com/diaspora/diaspora_federation"
  s.licenses = ["AGPL-3.0"]
  s.require_paths = ["lib"]
  s.rubygems_version = "1.8.23"
  s.summary = "diaspora* federation json schemas"

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
    else
    end
  else
  end
end
